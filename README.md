<div align="center">

<img src="./assets/RnOS_Polygon_Final.png" width=200 height=200>

<br>

<img src="./assets/Welcome.png" width="850" height="319">

</div>

# Why should you try RnOS ?

- A `systemd` service that tweaks the `picom` installation, based on whether the ISO boots on actual hardware or
on a VM (Virtual Machine).
- Pre-configured neovim configuration, written in `lua`, with all the necessary plugins.
- Comes with an amazing Window Manager - [Qtile](https://github.com/qtile/qtile), which is written and configured
in `python`, a language that a lot of people are familiar with, hence giving the user, immense scope for customization.
- Along, with Qtile, the extra modules written by [elParaguayo](https://github.com/elParaguayo) are also included, that
provide a wide variety of options to create a desktop that is tailored to your needs.
- Apart from this, there are a lot of utility scripts, that utilize rofi, to provide a user interface for easy user interaction.
To list a few, the `wifi_menu` and the `power_menu` scripts, use `rofi`.
- The `wal-set` script, also utilizes `rofi`, which in-turn uses `pywal` to change the colorscheme based 
on the selected wallpaper. This script, changes colors for the Qtile Bar, `dunst`, `rofi`, `betterlockscreen` background, `cava`,
and the terminal.
- Easy Installation with `calamares`.
- Comes pre-installed with `yay` as the AUR Helper.
- Finally, *RnOS* aims at providing a starting point for new to Linux users, to begin their journey on WMs and learn while 
exploring.

> The username and password of the live user is : `rnos`. Check out the complete installation instructions [here](https://gitlab.com/ruturajn/RnOS_Calamares_Config/-/blob/main/assets/Calamares_Demo.mp4), and the docs for the `Welcome-App` [here](https://gitlab.com/ruturajn/RnOS_Welcome_App). Also, a demo video for RnOS can be found [here](https://gitlab.com/ruturajn/RnOS_ISO/-/blob/main/assets/RnOS_Final_Demo.mp4).

<br>

*Head over to the [Releases](https://gitlab.com/ruturajn/RnOS_ISO/-/releases) Page, to grab the ISO. The recommended storage
and RAM for smoothly running RnOS is 20G and 4G respectively.*

***Note: This is still Beta Software, so I would love to get some more feedback and testing done on this. Please feel free to
create `Pull Requests` and `Issues`. Also, this software does not come with any sort of warranty whatsoever.***

## Acknowledgements

- Thanks to [Eznix's](https://www.youtube.com/c/eznix) Ezarcher project, which helped me with the `Calamares`
configuration, its `PKGBUILD`, and also understanding the basics of builiding a custom Arch based ISO.
- Thanks to [Erik Dubois](https://www.youtube.com/channel/UCJdmdUp5BrsWsYVQUylCMLg) and ArcoLinux, for
helping me in getting a grasp on creating a custom local repository, managing it, and
also writing a pacman hook, for the `lsb-release` package.
- Thanks to the people in the `#archlinux-releng` IRC, for helping me solve the `setup.service` issue.
- Thanks to [adi1090x's](https://github.com/adi1090x) `Archcraft` Project, for providing a reference to the boot
parameters, and `nvidia` configuration.
- Thanks to the people in the `calamares` chat room (on Matrix), for helping me out with the various design and
presentation aspects, for `calamares`.

## Authors

Ruturajn \<nanotiruturaj@gmail.com>
